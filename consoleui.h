#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>
#include "ScientistService.h"
#include <vector>
#include <QDate>

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
private:
    ScientistService service;
    void add();
    void show();
    void showNames();
    void remove();
    void search();
    void reset();
    void clear();
    void printScientistVector (vector<Scientist> scientistVector);
};

#endif // CONSOLEUI_H
