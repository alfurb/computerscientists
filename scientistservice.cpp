#include "scientistservice.h"

ScientistService::ScientistService()
{

}

void ScientistService::add(Scientist s) {
    scientistRepo.addToRepository(s);
}

vector<Scientist> ScientistService::getAll()
{
    return scientistRepo.getAll();
}

int compareByName(const Scientist& a, const Scientist& b)
{
    return a.getName() < b.getName();
}

bool compareByGender(const Scientist& a, const Scientist& b)
{
    return a.getGender() < b.getGender();
}

bool compareByBirth(const Scientist& a, const Scientist& b)
{
    return a.getYearOfBirth() < b.getYearOfBirth();
}

bool compareByDeath(const Scientist& a, const Scientist& b)
{
    return a.getYearOfDeath() < b.getYearOfDeath();
}

vector<Scientist> ScientistService::sortBy(vector<Scientist> inputVector, std::string input)
{
    if(input == "birth" || input == "yearOfBirth"){
        std::sort(inputVector.begin(), inputVector.end(), compareByBirth);
    } else if(input == "gender"){
        std::sort(inputVector.begin(), inputVector.end(), compareByGender);
    } else if(input == "death" || input == "yearOfDeath"){
        std::sort(inputVector.begin(), inputVector.end(), compareByDeath);
    } else {
        std::sort(inputVector.begin(), inputVector.end(), compareByName);
    }
    return inputVector;
}

bool ScientistService::remove(string name)
{
    return scientistRepo.removeFromRepository(name);
}

vector<Scientist> ScientistService::searchByName(std::string name)
{
    return scientistRepo.searchByName(name);
}

vector<Scientist> ScientistService::searchByBirth(int year)
{
    return scientistRepo.searchByBirth(year);
}

vector<Scientist> ScientistService::searchByDeath(int year)
{
    return scientistRepo.searchByDeath(year);
}

vector<Scientist> ScientistService::searchByGender(std::string gender)
{
    return scientistRepo.searchByGender(gender);
}

void ScientistService::clear()
{
    scientistRepo.clear();
}

bool ScientistService::checkIfNameInUse (std::string name)
{
    vector<Scientist> scientistVector = scientistRepo.getAll();
    for(unsigned int i = 0; i < scientistVector.size(); i++){
        if (scientistVector[i].getName() == name){
            return true;
        }
    }
    return false;
}

