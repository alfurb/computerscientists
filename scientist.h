#ifndef SCIENTIST_H
#define SCIENTIST_H

#include <QDate>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

class Scientist
{
private:
    std::string name;
    std::string gender;
    int yearOfBirth;
    int yearOfDeath;
    bool alive;
public:
    Scientist();
    void setName(std::string);
    std::string getName() const;
    void setGender(std::string);
    std::string getGender() const;
    void setYearOfBirth(int);
    int getYearOfBirth() const;
    void setYearOfDeath(int);
    int getYearOfDeath() const;
    bool isAlive() const;
    friend std::ostream& operator <<(std::ostream& outs, const Scientist& s);
    friend std::istream& operator >>(std::istream& ins, Scientist& s);
};



#endif // SCIENTIST_H
