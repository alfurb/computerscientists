#ifndef SCIENTISTREPOSITORY_H
#define SCIENTISTREPOSITORY_H

#include <vector>
#include <fstream>
#include <string>
#include "Scientist.h"

using namespace std;

class ScientistRepository
{
private:
    vector<Scientist> scientistVector;
    unsigned int noOfScientists;
    std::string path;
    std::ifstream ifile;
    std::ofstream ofile;
    void readAllScientist();
    void updateStorageFile();
    void add(Scientist s);
    bool remove(std::string name);
public:
    ScientistRepository();
    void addToRepository(Scientist s);
    vector<Scientist> getAll();
    bool removeFromRepository(std::string name);
    vector<Scientist> searchByName(std::string name);
    vector<Scientist> searchByBirth(int year);
    vector<Scientist> searchByDeath(int year);
    vector<Scientist> searchByGender(std::string gender);
    void clear();

};

#endif // SCIENTISTREPOSITORY_H
