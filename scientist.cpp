#include "scientist.h"

Scientist::Scientist()
{
    name = "";
    gender = "";
    yearOfBirth = 0;
    yearOfDeath = 0;
    alive = true;

}

void Scientist::setName(std::string input)
{
    name = input;
}

std::string Scientist::getName() const
{
    return name;
}

void Scientist::setGender(std::string input)
{
    gender = input;
}

std::string Scientist::getGender() const
{
    return gender;
}

void Scientist::setYearOfBirth(int input)
{
    yearOfBirth = input;
}

int Scientist::getYearOfBirth() const
{
    return yearOfBirth;
}

void Scientist::setYearOfDeath(int input)
{
    yearOfDeath = input;
    if(yearOfDeath == 0){
        alive = true;
    } else {
        alive = false;
    }
}

int Scientist::getYearOfDeath() const
{
    return yearOfDeath;

}

bool Scientist::isAlive() const
{
    return alive;
}

std::ostream& operator <<(std::ostream& outs, const Scientist& s)
{
    outs << "Name: " << s.getName() << "\n";
    outs << "Gender: " << s.getGender() << "\n";
    outs << s.getYearOfBirth() << " -";

    if(!s.isAlive()){
        outs << " " << s.getYearOfDeath();
    }

    outs << "\n";

    return outs;
}

std::istream& operator >>(std::istream& ins, Scientist& s)
{
    std::string tempString;

    getline(ins, tempString);

    s.setName(tempString.substr(6)); //substr to cut "Name: " from the front

    getline(ins, tempString);
    s.setGender(tempString.substr(8)); //substr to cut "Gender: " from the front

    int tempInt = 0;

    getline(ins, tempString);

    for(int i = 0; tempString[i] != ' '; i++){
        tempInt = tempInt * 10;
        tempInt += tempString[i] - '0';
    }

    s.setYearOfBirth(tempInt);
    tempInt = 0;

    if(tempString.length() > 6){
        getline(ins, tempString);
        for(int i = 0; tempString[i + 7] != ' '; i++){
            tempInt = tempInt * 10;
            tempInt += tempString[i + 7] - '0';
        }

        s.setYearOfDeath(tempInt);
        tempInt = 0;
    }

    return ins;
}
