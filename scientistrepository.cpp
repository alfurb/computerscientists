#include "scientistrepository.h"



ScientistRepository::ScientistRepository()
{
    path = "storageFile.txt";
    noOfScientists = 0;
    readAllScientist();
}

void ScientistRepository::addToRepository(Scientist s)
{
    add(s);

    updateStorageFile();
}

void ScientistRepository::readAllScientist()
{
    ifile.open(path.c_str());
    if(ifile.is_open()){
        Scientist temp;
        int quantityInFile = 0;
        ifile >> quantityInFile;
        ifile.get();
        for(int i = 0; i < quantityInFile; i++){
            ifile >> temp;
            if(scientistVector.size() > noOfScientists){
                scientistVector[noOfScientists] = temp;
            } else {
                scientistVector.push_back(temp);
            }

            noOfScientists++;
        }
        ifile.close();
    } else {
        std::cout << "Error opening ifile at readAllScientists";
    }
}

void ScientistRepository::updateStorageFile()
{
    ofile.open(path.c_str());

    if(ofile.is_open()){
        ofile.clear();
        ofile << noOfScientists << std::endl;

        for(unsigned int i = 0; i < noOfScientists; i++){
            ofile << scientistVector[i];
        }
        ofile.close();
    } else {
        std::cout << "Error opening file at updateStorageFile()";
    }
}

void ScientistRepository::add(Scientist s)
{
    if(scientistVector.size() > noOfScientists){
        scientistVector[noOfScientists] = s;
    } else {
        scientistVector.push_back(s);
    }

    noOfScientists ++;
}

bool ScientistRepository::remove(std::string name)
{
    for(unsigned int i = 0; i < noOfScientists; i++){
        if(scientistVector[i].getName() == name){
            scientistVector.erase(scientistVector.begin() + i);
            noOfScientists --;
            return true;
        }
    }
    return false;
}

vector<Scientist> ScientistRepository::getAll()
{
    return scientistVector;
}

bool ScientistRepository::removeFromRepository(std::string name)
{
    bool removed = remove(name);

    updateStorageFile();

    return removed;
}

vector<Scientist> ScientistRepository::searchByName(std::string name)
{
    vector<Scientist> temp;
    for(unsigned int i = 0; i < noOfScientists; i++){
        if(scientistVector[i].getName().find(name) != std::string::npos){
            temp.push_back(scientistVector[i]);
        }
    }

    return temp;
}

vector<Scientist> ScientistRepository::searchByBirth(int year)
{
    vector<Scientist> temp;
    for(unsigned int i = 0; i < noOfScientists; i++){
        if(scientistVector[i].getYearOfBirth() == year){
            temp.push_back(scientistVector[i]);
        }
    }

    return temp;
}

vector<Scientist> ScientistRepository::searchByDeath(int year)
{
    vector<Scientist> temp;
    for(unsigned int i = 0; i < noOfScientists; i++){
        if(scientistVector[i].getYearOfDeath() == year){
            temp.push_back(scientistVector[i]);
        }
    }

    return temp;
}

vector<Scientist> ScientistRepository::searchByGender(std::string gender)
{
    vector<Scientist> temp;
    for(unsigned int i = 0; i < noOfScientists; i++){
        if(scientistVector[i].getGender() == gender){
            temp.push_back(scientistVector[i]);
        }
    }

    return temp;
}

void ScientistRepository::clear()
{
    ofile.open(path.c_str());

    if(ofile.is_open()){
        ofile.clear();
        ofile << 0 << std::endl;
        ofile.close();
        scientistVector.clear();
        noOfScientists = 0;
    } else {
        std::cout << "Error opening file at clear()";
    }

}
