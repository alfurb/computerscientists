#include "consoleui.h"

ConsoleUI::ConsoleUI()
{

}

void ConsoleUI::start()
{
    std::string inp;
    std::cout << "Welcome!!!" << std::endl;
    do{
        std::cout << "The commands are:  add, remove, show, clear, search and exit" << std::endl;

        std::cout << "Please choose one: ";
        std::cin >> inp;

        if(inp == "add") {
            add();
        } else if(inp == "show") {
            show();
        } else if(inp == "search"){
            search();
        } else if(inp == "remove"){
            remove();
        } else if(inp == "clear"){
            clear();
        } else if(inp == "exit"){
            char input;
            std::cout << "Are you sure you want to Exit? Y/N: ";
            std::cin >> input;
            if(input == 'y' || input == 'Y')
                break;
        }

        else
        {
            std::cout <<std::endl;
            std::cout <<"Invalid input, please try again.";
            std::cout <<std::endl;
        }

        std::cout << std::endl;
    }while(true);
}
void ConsoleUI::add()
{
    Scientist s = Scientist();
    int yearOfBirth = 0, yearOfdeath = 0;
    std::string name, gender;
    std::cout << std::endl;
    std::cout << std::string(64, '-') << std::endl;

    std::cin.ignore();

    std::cout << "Input full name: ";
    std::getline(std::cin, name);
    while(service.checkIfNameInUse(name)){
        std::cout << "There is already an individual with that name." << std::endl;
        std::cout << "Try again. Full name: ";
        std::getline(std::cin, name);
    }
    s.setName(name);

    std::cout << "Gender(male/female): ";
    std::cin >> gender;
    while(!(gender == "male" || gender == "female"))
    {
        std::cout << "Invalid input, please choose male/female: ";
        std::cin >> gender;
    }
    s.setGender(gender);

    std::cout << "Year of birth (only positive integers): ";
    std::cin >> yearOfBirth;

    while(yearOfBirth < 0)
    {
        std::cout << "Invalid input, please try again: ";
        std::cin.ignore();
        std::cin >> yearOfBirth;
    }
    s.setYearOfBirth(yearOfBirth);

    std::cout << "Year of death (enter 0 if individual is still alive, only positive integers): ";

    std::cin >> yearOfdeath;
    while(yearOfdeath < yearOfBirth && yearOfdeath != 0){
        std::cout << "Invalid input, please try again: ";
        std::cin.ignore();
        std::cin >> yearOfdeath;
    }
    s.setYearOfDeath(yearOfdeath);

    service.add(s);
    std::cout << std::string(64, '-') << std::endl;
    std::cin.ignore();
}

void ConsoleUI::printScientistVector (vector<Scientist> scientistVector)
{
    for(unsigned int i = 0; i < scientistVector.size(); i++){
        std::cout << scientistVector[i] << std::endl;
    }
}

void ConsoleUI::showNames()
{
    vector<Scientist> temp;
    temp = service.sortBy(service.getAll(), "name");

    std::cout << string(40, '-') << std::endl;

    for (unsigned int i = 0; i < temp.size(); i++){
        std::cout << temp[i].getName() << std::endl;
    }

    std::cout << string(40, '-') << std::endl;
}

void ConsoleUI::search()
{
    std::cout << std::endl;
    std::cout << std::string(64, '-') << std::endl;
    std::string input;
    std::cout << "By which field would you like to search your scientists?" << std::endl;
    std::cout << "(name, gender, birth or death) Default: name" << std::endl;
    std::cout << "Search by: ";
    std::cin >> input;
    std::cout << std::endl;

    vector<Scientist> scientistVector;

    if(input == "gender")
    {
        std::string gender;
        std::cout << "Enter gender (female/male): ";
        std::cin.ignore();
        std::getline(std::cin, gender);
        std::cout << std::endl;
        scientistVector = service.searchByGender(gender);
    }
    else if(input == "birth")
    {
        int birth;
        std::cout << "Enter year of birth: ";
        std::cin.ignore();
        std::cin >> birth;
        std::cout << std::endl;
        scientistVector = service.searchByBirth(birth);
    }
    else if(input == "death")
    {
        int death;
        std::cout << "Enter year of death: ";
        std::cin.ignore();
        std::cin >> death;
        std::cout << std::endl;
        scientistVector = service.searchByDeath(death);
    } else
    {
        std::string name;
        std::cout << "Enter name: ";
        std::cin.ignore();
        std::getline(std::cin, name);
        std::cout << std::endl;
        scientistVector = service.searchByName(name);
    }

    std::cout << "How would you like to sort the resault?"<<std::endl;
    std::cout << "(name, gender, birth or death) Default: name" << std::endl;
    std::cout << "Sort by: ";

    std::cin >> input;
    std::cout << std::endl;

    printScientistVector(service.sortBy(scientistVector, input));

    std::cout << std::string(64, '-') << std::endl;

}

void ConsoleUI::show()
{
    std::cout << std::endl;
    std::string input;

    std::cout << std::string(64, '-') << std::endl;
    std::cout << "How would you like to sort the list?"<<std::endl;
    std::cout << "(name, gender, birth or death) Default: name" << std::endl;
    std::cout << "Sort by: ";

    std::cin >> input;
    std::cout << std::endl;
    std::cout << std::string(64, '-') << std::endl;

    printScientistVector(service.sortBy(service.getAll(), input));

    std::cout << std::string(64, '-') << std::endl;
}

void ConsoleUI::remove()
{
    std::cout << std::endl;
    std::cout << "Which Scientist would you like to remove?" << std::endl;
    showNames();
    std::cout << "Full name: ";

    std::string name;

    std::cin.ignore();
    getline(std::cin, name);

    if(service.remove(name)){
        std::cout << name << " removed" << std::endl;
    } else {
        std::cout << "Error removing scientist." << std::endl;
    }
}

void ConsoleUI::clear()
{
    char input;
    std::cout << "Are you sure you want to Clear the whole list? Y/N: ";
    std::cin >> input;

    if(input == 'y' || input == 'Y'){
        std::cout <<std::endl;
        std::cout << "Your list has been cleared." << std::endl;
        service.clear();
    }
}

