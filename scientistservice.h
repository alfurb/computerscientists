#ifndef SCIENTISTSERVICE_H
#define SCIENTISTSERVICE_H

#include "Scientist.h"
#include "Scientistrepository.h"
#include <string>

class ScientistService
{
private:
    ScientistRepository scientistRepo;
public:
    ScientistService();
    void add(Scientist s);
    vector<Scientist> getAll();
    vector<Scientist> sortBy(vector<Scientist> inputVector, std::string input);
    bool remove(std::string name);
    vector<Scientist> searchByName(std::string name);
    vector<Scientist> searchByBirth(int year);
    vector<Scientist> searchByDeath(int year);
    vector<Scientist> searchByGender(std::string gender);
    void clear();
    bool checkIfNameInUse(std::string name);
};

#endif // SCIENTISTSERVICE_H
