# README #

### Welkomin ###

* Verkefnið fyrir 3ja vikna verklegt námskeið á 1. önn jól 2014
* Version 1
* storageFile.txt þarf að vera í sömu möppu og ComputerScientists executable skráin.
* Ef upp kemur villa áður en línan Welcome!!! er prentuð þarf að fara í gegnum add ferlið og loka forritinu. Þá er búin til ný storageFile.txt skrá sem þarf að skipta út fyrir þá sem var send inn. Þegar forritið er keyrt upp aftur ætti allt að ganga.
* Ef það gengur ekki heldur hvet ég ykkur til að keyra skipanirnar add, remove og show nokkrum sinnum og sjá að öllu sem þið gerið er þrátt fyrir allt haldið til haga.
* Ef ekkert af ofangreindu gengur má hafa samband við okkur til að sjá virkni forritsins í okkar tölvum.

### Meðlimir hóps 49 ###

* Álfur Birkir Bjarnason
* Friðrik Þór Hjálmarsson
* Tómas Tryggvason